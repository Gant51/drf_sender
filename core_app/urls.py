from django.urls import path, include
from rest_framework.routers import DefaultRouter
from core_app import views

router = DefaultRouter()
router.register(r'mailing', views.MailingViewSet, basename="mailing",)
router.register(r'client', views.ClientViewSet, basename="client")
router.register(r'statistics', views.StatisticsViewSet, basename="statistics")

urlpatterns = [
    path('', include(router.urls)),
]
