import datetime
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from . import functions
from core_app.serializers import MailingSerializer, ClientSerializer, StatisticsSerializer, DetailStatisticsSerializer
from core_app.models import Mailing, Client, Message


class ClientViewSet(viewsets.ModelViewSet):
    """
    list:
    Return information about all clients

    create:
    Add a new client

    retrieve:
    Return information about specific client

    update:
    Updates all client parameters

    partial_update:
    Updates certain client parameters

    destroy:
    Delete the client
    """

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingViewSet(viewsets.ModelViewSet):
    """

    list:
    Return information about all mailings

    retrieve:
    Return information about specific mailing

    update:
    Updates all mailing parameters

    partial_update:
    Updates certain mailing parameters

    destroy:
    Delete the mailing

    """

    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def create(self, request, *args, **kwargs):
        """
        When adding a new mailing, if current time more then date start and less then date end,
        create messages and send them.
        """
        result = super(MailingViewSet, self).create(request, *args, **kwargs)
        date_end = datetime.datetime.strptime(result.data['date_end'], '%Y-%m-%dT%H:%M:%SZ')
        date_start = datetime.datetime.strptime(result.data['date_start'], '%Y-%m-%dT%H:%M:%SZ')
        mailing = Mailing.objects.get(pk=result.data['pk'])
        if date_start.timestamp() < functions.current_date.timestamp() < date_end.timestamp():
            functions.mailing_activate(mailing.pk)
        elif date_start.timestamp() > functions.current_date.timestamp():
            functions.plan_the_mailing(mailing, datetime.datetime.fromtimestamp(functions.current_date.timestamp()))
        return result


class StatisticsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Get general statistics
    """
    queryset = Mailing.objects.all()
    serializer_class = StatisticsSerializer

    @action(detail=True)
    def details(self, request, *args, **kwargs):
        """
        Get detailed statistics
        """
        queryset = Message.objects.filter(mailing=kwargs['pk'])
        self.serializer_class = DetailStatisticsSerializer
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
