import datetime
import requests

from loguru import logger
from django.db.models import Q
from django.utils import timezone

from core_app.models import Mailing, Client, Message
from drf_sender.celery import app

current_date = timezone.now()

logger.add('errors.log', format="{time} {level} {message}")


def plan_the_mailing(mailing, date):
    """
    Planing mailing to activate in future
    """

    mailing_activate.apply_async((mailing.pk,), eta=date)


# for testing without sending:
# import random
# @app.task
# def send_message(mail):
#     """
#     For testing:
#     Generate random status without sending message.
#     """
#     status = random.randrange(1, 3)
#     return status

@app.task
def send_message(data_for_send):
    request_url = 'https://probe.fbrq.cloud/v1/send/{}'.format(data_for_send['id'])
    headers = {
        'accept': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
                         '.eyJleHAiOjE2Nzk0MDMwNDUsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlZsYWRpc2xhdiJ9'
                         '.bP5_okGBmU9nBf5S4ihk1hkVMjXSvb2sAjyP84OXGT4',
    }
    response = requests.post(request_url, headers=headers, json=data_for_send)
    status = 1 if response.status_code == 200 else 2
    return status


@app.task
def mailing_activate(mailing_pk):
    mailing = Mailing.objects.get(pk=mailing_pk)
    send_in_future_flag = False
    """
    Get mailing, as argument. Find all clients with operator code and tag as in mailing.
    For each client check, if exists messages satisfying to him and this newsletter,
    If not, create and send it
    If exist, but status not "success", send it
    """

    clients = Client.objects.filter(tag=mailing.filter_tag)
    for client in clients:
        message = get_message(client, mailing)
        phone = client.phone_num
        phone_str = '{}{}'.format(phone.country_code, phone.national_number)
        data_for_send = {
            "id": message.id,
            "phone": int(phone_str),
            "text": mailing.message_text}
        status = send_message(data_for_send)
        if status == 1:
            message.date_send = current_date
        if status == 2:
            send_in_future_flag = True
        message.status = status
        message.save()

    if send_in_future_flag:
        date = current_date + datetime.timedelta(minutes=10)
        plan_the_mailing(mailing, date)


def get_message(client, mailing):
    messages = Message.objects.filter(Q(mailing=mailing.pk) & Q(client=client.pk))
    if len(messages) == 1 and messages[0].status != 1:
        message = messages[0]
    elif len(messages) == 0:
        message = Message(date_create=current_date, status=0, mailing=mailing, client=client)
    else:
        logger.error("Multiply messages")
        message = False

    if message:
        message.save()
    return message
