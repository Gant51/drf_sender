from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from core_app.settings import TIME_ZONES, MESSAGE_STATUSES


class Mailing (models.Model):
    date_start = models.DateTimeField()
    message_text = models.TextField()
    filter_tag = models.CharField(max_length=16)
    filter_mobile_operator_code = models.IntegerField()
    date_end = models.DateTimeField()

class Client (models.Model):
    phone_num = PhoneNumberField(null=False, blank=False, unique=True)
    mobile_operator_code = models.IntegerField()
    tag = models.CharField(max_length=16)
    time_zone = models.CharField(max_length=32, choices=TIME_ZONES)


class Message (models.Model):
    date_create = models.DateTimeField(auto_now_add=True)
    date_send = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(choices=MESSAGE_STATUSES)
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', on_delete=models.CASCADE)

