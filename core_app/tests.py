from django.test import TestCase


class ClientViewSetTest(TestCase):

    def test_creating_client(self):
        """
            Creating and receiving a client
        """
        new_client = self.client.post('/client/', {
            'phone_num': '+79999999999',
            'mobile_operator_code': 999,
            'tag': 'test_tag',
            'time_zone': 'Africa/Abidjan',
        })
        created_client = self.client.get('/client/1/')
        self.assertIs(new_client.status_code == 201 and created_client.status_code == 200, True)

    def test_put_client(self):
        """
           Updating all client parameters
        """

        client_data = {
            'phone_num': '+79999999999',
            'mobile_operator_code': 999,
            'tag': 'test_tag',
            'time_zone': 'Africa/Abidjan',
        }
        self.client.post('/client/', client_data)
        client_data['tag'] = 'test_tag_new'
        updated_client = self.client.put('/client/1/', client_data, content_type='application/json')
        self.assertIs(updated_client.status_code == 200, True)

    def test_patch_client(self):
        """
            Updating certain client parameters
        """

        client_data = {
            'phone_num': '+79999999999',
            'mobile_operator_code': 999,
            'tag': 'test_tag',
            'time_zone': 'Africa/Abidjan',
        }
        self.client.post('/client/', client_data)
        client_data = {'tag': 'test_tag_new'}
        updated_client = self.client.patch('/client/1/', client_data, content_type='application/json')
        self.assertIs(updated_client.status_code == 200, True)

    def test_delete_client(self):
        """
            Deleting certain client parameters
        """

        client_data = {
            'phone_num': '+79999999999',
            'mobile_operator_code': 999,
            'tag': 'test_tag',
            'time_zone': 'Africa/Abidjan',
        }

        self.client.post('/client/', client_data)
        self.client.get('/client/1/')
        deleted_client = self.client.delete('/client/1/')
        self.assertIs(deleted_client.status_code == 204, True)


class MailingViewSetTest(TestCase):

    def test_creating_mailing(self):
        """
            Creating and receiving a mailing
        """
        mailing_data = {
            'date_start': '2022-06-08 12:00:00',
            'message_text': 'test',
            'filter_tag': 'test_tag',
            'filter_mobile_operator_code': '925',
            'date_end': '2022-06-08 12:00:00',
        }
        new_mailing = self.client.post('/mailing/', mailing_data)
        created_mailing = self.client.get('/mailing/1/')
        self.assertIs(new_mailing.status_code == 201 and created_mailing.status_code == 200, True)

    def test_put_mailing(self):
        """
           Updating all mailing parameters
        """

        mailing_data = {
            'date_start': '2022-06-08 12:00:00',
            'message_text': 'test',
            'filter_tag': 'test_tag',
            'filter_mobile_operator_code': '925',
            'date_end': '2022-06-08 12:00:00',
        }
        self.client.post('/mailing/', mailing_data)
        mailing_data['tag'] = 'test_tag_new'
        updated_mailing = self.client.put('/mailing/1/', mailing_data, content_type='application/json')
        self.assertIs(updated_mailing.status_code == 200, True)

    def test_patch_mailing(self):
        """
            Updating certain mailing parameters
        """

        mailing_data = {
            'date_start': '2022-06-08 12:00:00',
            'message_text': 'test',
            'filter_tag': 'test_tag',
            'filter_mobile_operator_code': '925',
            'date_end': '2022-06-08 12:00:00',
        }
        self.client.post('/mailing/', mailing_data)
        mailing_data = {'tag': 'test_tag_new'}
        updated_mailing = self.client.patch('/mailing/1/', mailing_data, content_type='application/json')
        self.assertIs(updated_mailing.status_code == 200, True)

    def test_delete_mailing(self):
        """
            Deleting certain mailing parameters
        """

        mailing_data = {
            'date_start': '2022-06-08 12:00:00',
            'message_text': 'test',
            'filter_tag': 'test_tag',
            'filter_mobile_operator_code': '925',
            'date_end': '2022-06-08 12:00:00',
        }

        self.client.post('/mailing/', mailing_data)
        self.client.get('/mailing/1/')
        deleted_mailing = self.client.delete('/mailing/1/')
        self.assertIs(deleted_mailing.status_code == 204, True)
