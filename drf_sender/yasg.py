from django.urls import path
from rest_framework import permissions
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="DRF Sender",
        default_version='v1',
        description="""
        A service for creating and managing newsletters.
        After creating a new mailing list and the start time arrives, all clients that match the values of the filter specified in this mailing list are selected and sending is started for all these clients.
        If for some reason the messages are not sent out before the end of the mailing, no messages will be sent after that time.
        In the course of sending messages, statistics are collected for each message for subsequent generation of reports.
        """,
    ),
    public=True,
    permission_classes=(permissions.AllowAny,)
)

urlpatterns = [
    path('swagger(?P<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
