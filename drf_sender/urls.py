
from django.contrib import admin
from django.urls import path, include
from rest_framework.schemas import get_schema_view
from .yasg import urlpatterns as doc_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('core_app.urls')),
    path('openapi/', get_schema_view(
        title="Notifications sender",
        description="API for creating and sending mailing of notifications",
        version="1.0.0"
    ), name='openapi-schema'),
]

urlpatterns += doc_urls
